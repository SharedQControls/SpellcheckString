<h1>Description</h1>

The SpellcheckString is a QControl and requires the QControl Toolkit, http://bit.ly/QControlNITools.  It inherits from and extends the String control.  It implements run-time spell checking inside of a String control.

It highlights misspelled words by underlining and changing the color to red.  Right-clicking on a misspelled word opens a menu with the suggestions and an option to add word to dictionary.
- Selecting a suggestion replaces the word for the suggestion
- Selecting "Add to Dictionary" adds the word and removes the highlight

<h1>LabVIEW Version</h1>

This code is currently published in LabVIEW 2018

<h1>Build Instructions</h1>

The <b>SpellcheckString.lvclass</b> and all its members; the <b>SpellcheckEngine.lvclass</b>, its child classes and all its members; and the <b>Dictionary.lvclass</b>, its child classes and all its members can be distributed with a project and built into the using application.  

The <b>SpellcheckEngine.lvclass</b>, its child classes and all its members; and the <b>Dictionary.lvclass</b>, its child classes and all its members can be used separate from the <b>SpellcheckString.lvclass</b> if it is desired to implement in another way.

<h1>Installation Guide</h1>

The <b>SpellcheckString.lvclass</b> and all its members; the <b>SpellcheckEngine.lvclass</b>, its child classes and all its members; and the <b>Dictionary.lvclass</b>, its child classes and all its members can be distributed with a project and built into the using application.  

The <b>SpellcheckEngine.lvclass</b>, its child classes and all its members; and the <b>Dictionary.lvclass</b>, its child classes and all its members can be used separate from the <b>SpellcheckString.lvclass</b> if it is desired to implement in another way.

<h1>Execution</h1>
See documentation distributed with the QControl Toolkit.

<h1>Support</h1>
Submit Issues or Merge Requests through GitLab.